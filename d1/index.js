console.log("HEllo World");

// SECTION - Objects
/* 
-objects are data types that are used to represent real world objects
-collection of related data and/or functionalities
In JavaScript, most features like strings and arrays are considered to be objects
-their difference to JavaScript objects, is that the JS object has a "key: value" pair
-keys are also referred to as properties while the value is the figure
-objects are commonly initialized or declared using the let + objectName and this object will start with curly brace {} which is also called as "Object Literals"

SYNTAX(using initializers): 
    let/const objectName = {
        keyA: valueA,
        keyB: valueB
    }
*/

let cellphone = {
    name: "Nokia 3210",
    manufacturedData: 1999
}

console.log("Result from creating objects using initializers");
console.log(cellphone);
console.log(typeof cellphone);

/* 
Miniactivity
create an object called car
name/model
release date
*/

// let car = {
//     model: "Suzuki Ciaz",
//     releaseDate: 2019
// }

// console.log("Result from creating objects using initializers");
// console.log(car);
// console.log(typeof car);

// Creating objects using constructor function
/* 
-creates a reusable function to create/construct several objects that have the same data structures
-can create mulitple objects with the same structure  
-this is useful for creating multiple instances/copies of an object
-instance is a concrete occurrence of any object which emphasized on the distinct/unique identity of it (how we instantiate an object)

SYNTAX: 
    function ObjectName(keyA,keyB){
        this.keyA = keyA;
        this,keyB = keyB
    }
*/

/* 
this keyword allows to assign a new property for the object by associating them with values received from a constructor function's parameters
*/


// original object - money plate
function Laptop (name, manufacturedDate){
    this.name = name;
    this.manufacturedDate = manufacturedDate;
}

// this is a unique instance/copy of the Laptop object
/* 
-"new" keyword creates an instance of an object
- objects and instances are often interchanged because object literals and instances are distinct/unique
-not using "new" keyword would return undefined if we try to log the instance in the console since we don't have return statement in the Laptop function
*/
// the money / product
let laptop = new Laptop("Dell",2012);
console.log("Result from creating objects using initializers")
console.log(laptop);
console.log(typeof laptop);

// creates a new Laptop object
let laptop2 = new Laptop("Lenovo", 2008);
console.log("Result from creating objects using initializers")
console.log(laptop2);
console.log(typeof laptop2);

// create empty objects
let computer = {};
let myComputer = new Object();
console.log("Result from creating objects using initializers")
console.log(myComputer);
console.log(typeof myComputer);

// Accessing Objects
/* 
SYNTAX: objectName.key
*/


// using dot notation
console.log("Result from dot notation: " + laptop.name);

// using square brackets
// console.log ("Result from square brackets notation: " + laptop[name]); - returns undefined since there is no element inside hte laptop than be accessed using square bracket notation

// array of objects
/* 
-accessing arrays would be the priority since that is the first data type to access
-we'll use laptops[1] to access laptop2 inside the laptops array
-since the laptop2 is an object, we have to use dot notation to access it propoerties(name) and get their value

SYNTAX:
    arrayName[index].property
*/
let laptops = [laptop,laptop2];
console.log(laptops[1].name);
// accessing laptop array - laptop - manufacturedDate
console.log(laptops[0].manufacturedDate);
// using string inside the second square bracket would also access the value of the property
console.log(laptops[0]["manufacturedDate"]);



// SECTION - initializing, adding, deleting and reassigning object properties
/* 
-like any other variables in JavaScript, objects may have their properties initialized or added after the object was created/declared
-this is usefule for times when an object's properties are undetermined at the time of creating them
-we can update the object later

*/
let car = {};
console.log(car);
// adding properties and their values after declaring the variable
car.name = "Honda Vios"
car.manufacturedDate = 2022;
console.log(car);
/* 
-while using square brackets will give the same feature as using dot notation, it might lead us to create unconventional naming for the properties which might lead to future confusion when we try to access them
*/
car["manufactured date"] = 2019;
console.log(car);

// deleting of properties 
delete car['manufactured date'];
console.log(car);

// deletes properties using dot notation
// delete car.name;
// console.log(car);

// reassigning of properties
car.name = "Dodge Charger R/T";
console.log(car);

// Object Methods

/* 
-an object method is a function that is set by the dev to be the value of one of the properties
-they are also functions and one of the differences they have is that methods are functions related to a specific objects
-these are useful for creating object-specific functions which are used to perform tasks on them
-similar to functions/features of real world objects, methods are defined based on what an object is capable of doing and how they should do it.
-local to the object ang function
*/

let person = {
    name: "John",
    // talk's value is a function
    talk: function(){
        console.log("Hello! My name is " + this.name)
    },
    // walk: function(){
    //     console.log(this.name + " walked 25 steps")
    // }
}

console.log(person);
console.log("Result from object methods:");
// person.talk();

// adding a method - use a add property 
person.walk = function(){
    console.log(this.name + " walked 25 steps")
    // using template literals backticks + ${} + string data type
    // console.log( `${this.name} walked 25 steps`);
    
}
person.walk();

let friend = {
    firstName: "Joe",
    lastName: "Smith",
    // nested object
    address: {
        city: "Austin",
        state: "Texas"
    },
    // nested array
    emails: ['joe@mail.com',"johnHandsome@mail.com"],
    // object method
    introduce: function(){
        console.log("Hello! My name " + this.firstName + " " + this.lastName + ".");
    }
}

friend.introduce();
console.log(friend);

// real-world application of objects

/* 
Scenario
    -we would like to create a game that would have several pokemon interact with each other
    -every pokemon would have the same set of stats, properties and functions
*/

let myPokemon = {
    name: "Pikachu",
    level: 3,
    health: 100,
    attack: 50,
    tackle: function(){
        console.log("This Pokemon tackled targetPokemon");
        console.log("targetPokemon's health is now reduced to _targetPokemonHealth_");
    },
    faint: function(){
        console.log("Pokemon Fainted")
    }
};
console.log(myPokemon);

// using constructor function
function Pokemon (name, level){
    // properties
    this.name = name;
    this.level = level;
    this.health = 2*level;
    this.attack = level;

    // Methods
    this.tackle = function(target){
        console.log(this.name + " tackled " + target.name);
        console.log("targetPokemon's health is now reduced to _targetPokemonHealth_")
    };
    this.faint = function(){
        console.log(this.name + " fainted.")
    }
}

// creating new Pokemon

let snorlax = new Pokemon ("Snorlax",50);
lemachamp = new Pokemon ("Machamp",20);t 

// using tackle method from snorlax, with machamp as its target
snorlax.tackle(machamp);
