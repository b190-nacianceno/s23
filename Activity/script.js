// console.log("Hello World");

let trainer = {
    name: "Ash",
    age: 20,
    pokemon: ["Pikachu", "Snorlax", "Squirtle"],
    friends: {
        hoenn: ["Misty","May"],
        konto: ["Brock", "May"]
    },
    talk: function(){
        console.log("Gotta catch 'em all!");
    }
}
console.log(trainer);

// Dot Notation
console.log("Result of dot notation:");
console.log(trainer.age);

// Square Bracket Notation
console.log("Result of bracket notation:");
console.log(trainer.friends.konto[1]);



console.log("Result of talk method:");
console.log(trainer.talk());
// trainer.talk();
let snorlax = new Pokemon ("Snorlax",50);
let machamp = new Pokemon ("Machamp",20);
let squirtle = new Pokemon ("Squirtle",35);
let charmander = new Pokemon ("Charmander",25);
let gengar = new Pokemon ("Gengar",51);

console.log(snorlax);
console.log(machamp);
console.log(squirtle);
console.log(charmander);
console.log(gengar);

function Pokemon (name, level){
    // properties
    this.name = name;
    this.level = level;
    this.health = 2*level;
    this.attack = level;

    this.tackle = function(target){
        console.log(this.name + " tackled " + target.name);
        let targetPokemonHealth = target.health - this.attack;
        console.log(target.name + "'s health is now reduced to " + targetPokemonHealth);
        target.health = targetPokemonHealth
        if (targetPokemonHealth <= 0){
            // console.log("fainted");
            this.faint = function(){
                console.log(target.name + " fainted.")
                
            }
            this.faint();
        }
        console.log(target);
    };
}


snorlax.tackle(machamp);
machamp.tackle(snorlax);
gengar.tackle(charmander);
